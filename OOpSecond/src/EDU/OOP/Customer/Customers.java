package EDU.OOP.Customer;

import java.util.ArrayList;

import java.util.List;

public class Customers {
    final private String NameCustomer;
    public final ArrayList<Customer> customerArrayList = new ArrayList<>();

    public Customers(String nameCustomer) {
        NameCustomer = nameCustomer;
    }

    void addCustomerToCustomers(Customer customer) {
        customerArrayList.add(customer);
    }


    List<Customer> getListByName() {
        List<Customer> list = new ArrayList<>(customerArrayList);
        list.sort((o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
        return list;
    }


    List<Customer> getListByDiapasonCreditCard(int diapasonStart, int diapasonEnd) {
        List<Customer> list = new ArrayList<>();
        for (Customer customer : customerArrayList) {
            if (customer.getCardNumber() >= diapasonStart && customer.getCardNumber() <= diapasonEnd) {
                list.add(customer);
            }
        }

        return list;
    }


}
