package EDU.OOP.Customer;

public class Customer {
    private final String name;
    private final String surname;
    private final String patronymic;

    private final String address;

    private final int id;
    private final int cardNumber;
    private final int bankBill;

    public String getName() {
        return name;
    }

    public int getCardNumber() {
        return cardNumber;
    }

    public Customer(String name, String surname, String patronymic, String address, int id, int cardNumber, int bankBill) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.address = address;
        this.id = id;
        this.cardNumber = cardNumber;
        this.bankBill = bankBill;
    }

    public String toString() {
        return String.format("ID: %d\t name: %s\t name2: %s\t name3 %s \t from: %s\t credit card %d\t bank number %d",
                id, name, surname, patronymic, address, cardNumber, bankBill);
    }
}
