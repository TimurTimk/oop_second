package EDU.OOP.Student.Customer;

import java.util.List;


public class Test3 {
    public static void main(String[] args) {
        Customers clients = new Customers("clients ");
        clients.addCustomerToCustomers(new Customer("Андрей", "Фамильян", "Иванович", "Россия", 1, 22334455, 23456));
        clients.addCustomerToCustomers(new Customer("Иван", "Дроздов", "Михайлович", "Украина", 2, 11223344, 12345));
        clients.addCustomerToCustomers(new Customer("Богдан", "Мурахоедов", "Франкович", "Болгария", 3, 33445566, 34567));
        clients.addCustomerToCustomers(new Customer("Виктор", "Викторян", "Викторович", "Украина", 4, 44556677, 45678));
        clients.addCustomerToCustomers(new Customer("Франк", "Ибрагимов", "Ибрагимович", "Латвия", 5, 55667788, 56789));
        clients.addCustomerToCustomers(new Customer("Наташа", "Дроздова", "Алексеевна", "Россия", 6, 66778899, 67890));
        clients.addCustomerToCustomers(new Customer("Оксана", "Ростинова", "Ростиславовна", "Украина", 7, 77889900, 78901));
        clients.addCustomerToCustomers(new Customer("Степан", "Боруков", "Алексеевич", "Россия", 8, 88990011, 89012));
        clients.addCustomerToCustomers(new Customer("Кирилл", "Верховодко", "Сергеевич>", "Америка", 9, 999001122, 90123));
        clients.addCustomerToCustomers(new Customer("Александр", "Добрынин", "Станиславович", "Украина", 10, 10122334, 10234));

        System.out.println("\nsort by name:");

        List<Customer> listSortByName = clients.getListByName();
        for (Customer customer : listSortByName) {
            System.out.println(customer);
        }


        System.out.println("\nprint customers by diapason credit card:");
        int diapasonStart = 22_33_44_55;
        int diapasonEnd = 66_77_88_99;
        List<Customer> listSortByDiapasonCreditCard = clients.getListByDiapasonCreditCard(diapasonStart, diapasonEnd);
        for (Customer customer : listSortByDiapasonCreditCard) {
            System.out.println(customer);
        }


    }


}



