package EDU.OOP.Student.Student;

import EDU.OOP.Student.Student;

public class Main {
    public static void main(String[] args) {
        Student[] ourClass = new Student[10];
        ourClass[0] = new Student("Timur", "verhkovodko", 1, getArrOfScore());
        ourClass[1] = new Student("Taras", "Rybin", 2, getArrOfScore());
        ourClass[2] = new Student("Demid", "Grishanovich", 3, getArrOfScore());
        ourClass[3] = new Student("Danil", "Anischenka", 4, getArrOfScore());
        ourClass[4] = new Student("Dmitriy", "Potailo", 5, getArrOfScore());
        ourClass[5] = new Student("Matvey", "Pusko", 6, getArrOfScore());
        ourClass[6] = new Student("Daniil", "Rybin", 7, getArrOfScore());
        ourClass[7] = new Student("Genia", "Kovalchuk", 8, getArrOfScore());
        ourClass[8] = new Student("Geтia", "Litvinova", 9, getArrOfScore());
        ourClass[9] = new Student("Artut", "Podpalov", 10, getArrOfScore());
        for (int i = 0; i < 10; i++) {
            if (ourClass[i].checkGrades()) {
                ourClass[i].display();
            }
        }
    }
    public static int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
    public static int[] getArrOfScore() {
        int[] arr = new int[5];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = getRandomNumber(7, 11);
        }
        return arr;
    }
}


