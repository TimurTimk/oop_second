package EDU.OOP.Student.Student;
import java.util.Arrays;
public class Student {
    final private String name;
    final private String surname;
    final private int groupNumber;
    final private int[] grades;
    public Student(String name, String surname, int groupNumber, int[] score) {
        this.name = name;
        this.surname = surname;
        this.grades = score;
        this.groupNumber = groupNumber;
    }
    public void display(){
        System.out.print(this.name+" ");
        System.out.print(this.surname+" ");
        System.out.print(this.groupNumber+" ");
        System.out.print(Arrays.toString(this.grades) +" ");
        System.out.println();
    }
    boolean checkGrades() {
        for (int grade : grades) {
            if (grade != 9 && grade != 10) {
                return false;
            }
        }
            return true;
    }

}
