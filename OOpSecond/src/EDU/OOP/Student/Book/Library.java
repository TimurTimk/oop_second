package EDU.OOP.Student.Book;


import java.util.ArrayList;
import java.util.List;

class Library {

   final String libraryName;

    Library(String libraryName) {
        this.libraryName = libraryName;
    }

    public final ArrayList<Book> booksList = new ArrayList<>(); // наш лист
    void addBookToBookList(Book book) {
        booksList.add(book);
    }  //метод для добавленя обьектов в либрару
    List<Book> getListByAuthor(String author) {

        List<Book> list = new ArrayList<>();
        for (Book book : booksList) {
            if (book.getAuthor().equals(author)) {
                list.add(book);
            }
        }
        return list;
    }


    List<Book> getListByPublisher (String publisher) {
        List<Book> list = new ArrayList<>();
        for (Book book : booksList) {
            if (book.getPublisher().equals(publisher)) {
                list.add(book);
            }
        }
        return list;
    }


    List<Book> getListByYear(int year) {
        List<Book> list = new ArrayList<>();
        for (Book book : booksList) {
            if (book.getYear() >= year) {
                list.add(book);
            }
        }
        return list;
    }


}