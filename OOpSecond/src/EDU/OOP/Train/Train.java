package EDU.OOP.Train;

public class Train {
    private final String destinationName;
    private final int trainNumber;
    private final String departureTime;

    public String getDestinationName() {
        return destinationName;
    }

    public int getTrainNumber() {
        return trainNumber;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public Train(String destinationName, int trainNumber, String departureTime) {
        this.destinationName = destinationName;
        this.trainNumber = trainNumber;
        this.departureTime = departureTime;
    }

    public void display() {
        System.out.println("station:" + this.destinationName + " Train Number:" + this.trainNumber + " Departture Time:" + this.departureTime);
    }
}
