package EDU.OOP.Train;

import java.util.Scanner;
import java.util.Arrays;

public class Test2 {
    public static void main(String[] args) {
        Train[] ourTrain = new Train[5];
        ourTrain[0] = new Train("1st Staion", 2, "18:45");
        ourTrain[1] = new Train("2st Staion", 4, "13:60");
        ourTrain[2] = new Train("3st Staion", 3, "15:30");
        ourTrain[3] = new Train("3st Staion", 5, "17:15");
        ourTrain[4] = new Train("3st Staion", 1, "23:30");


        sortByNumber(ourTrain);//sort by Train Number
        displayAllValues(ourTrain);
        sortByDestination(ourTrain);//sort by dest name and be time
        displayAllValues(ourTrain);
        systemInStation(ourTrain);
    }

    public static void displayAllValues(Train[] ourTrain) {
        for (Train train : ourTrain) {
            train.display();
        }
        System.out.println();
    }

    public static void sortByNumber(Train[] ourTrain) {//sort by number
        Arrays.sort(ourTrain, (o1, o2) -> o1.getTrainNumber() - o2.getTrainNumber()); //условия для нового компоратора (заменил на лямду)
    }

    public static void sortByDestination(Train[] ourTrain) {//sort by dest name and be time
        Arrays.sort(ourTrain, (o1, o2) -> {
            int result = o1.getDestinationName().compareTo(o2.getDestinationName());
            if (result == 0) {
                return o1.getDepartureTime().compareTo(o2.getDepartureTime());
            }
            return result;
        });
    }

    public static void systemInStation(Train[] ourTrain) {
        Scanner scan = new Scanner(System.in);
        int input = scan.nextInt();
        for (Train train : ourTrain) {
            if (input == train.getTrainNumber()) {
                train.display();
                break;
            }
        }
    }
}
// Добавить возможность отображать информацию о поезде, номер которого вводит пользователь.


